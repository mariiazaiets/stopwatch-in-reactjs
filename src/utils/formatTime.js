export const formatTime = (stopwatch) => {
    const getSeconds = `0${(stopwatch % 60)}`.slice(-2);
    const minutes = `${Math.floor(stopwatch / 60)}`;
    const getMinutes = `0${minutes % 60}`.slice(-2);
    const getHours = `0${Math.floor(stopwatch / 3600)}`.slice(-2);

    return `${getHours} : ${getMinutes} : ${getSeconds}`;
};