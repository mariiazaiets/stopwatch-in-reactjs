import './App.css';
import {formatTime} from "./utils/formatTime";
import {useStopwatch} from "./hooks/useStopwatch";

const App = () => {
    const {stopwatch, startAndStopStopwatch, waitStopwatch, resetStopwatch} = useStopwatch();

    return (
        <div className='Wrapper'>
            <div className='App'>
                <h2>Stopwatch</h2>
                <div className='Stopwatch-box'>
                    <p>{formatTime(stopwatch)}</p>
                </div>
                <div className='Buttons'>
                    <button onClick={startAndStopStopwatch}>Start / Stop</button>
                    <button onClick={waitStopwatch}>Wait</button>
                    <button onClick={resetStopwatch}>Reset</button>
                </div>
            </div>
        </div>
    );
};

export default App;
