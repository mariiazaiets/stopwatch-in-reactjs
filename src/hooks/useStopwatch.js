import {useState, useRef} from 'react';

export const useStopwatch = () => {
    const [stopwatch, setStopwatch] = useState(0);
    const [isActive, setIsActive] = useState(false);
    const [clickTime, setClickTime] = useState(0);
    const refCount = useRef(null);

    const startAndStopStopwatch = () => {
        if ((isActive === false)) {
            setIsActive(true);
            refCount.current = setInterval(() => {
                setStopwatch((stopwatch) => stopwatch + 1);
            }, 1000)
        } else {
            clearInterval(refCount.current);
            setIsActive(false);
        }
    };

    const waitStopwatch = () => {
        // if delay was more then 300 ms reset the counter
        if (clickTime === 0) {
            setTimeout(() => {
                setClickTime(0);
            }, 300);
        }

        // increase click time by one
        setClickTime(clickTime + 1);

        // run the function if there were more few clicks);
        if (clickTime > 0) {
            clearInterval(refCount.current);
            setIsActive(false);
        }
    };

    const resetStopwatch = () => {
        clearInterval(refCount.current);
        setIsActive(false);
        setStopwatch(0);
    };

    return {stopwatch, startAndStopStopwatch, waitStopwatch, resetStopwatch};
};